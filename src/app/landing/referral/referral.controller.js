(function() {

'use strict';

angular
  .module('hoodooTwo')
  .controller('ReferralController', ReferralCtrl);

function ReferralCtrl($scope) {
  $scope.title = {
    'h1':'Got a referral?',
    'lead': 'Hoodoo values your time and would like to reward you for referring clients to us. If you happen to know of someone that is actively looking to embark upon a web development project, please submit the form below.',
    'disclaimer': 'Referral bonus is 10% of project price with a maximum benefit of $500 per project. All referral bonuses paid upon project completion and receipt of final payment.'

  };
  $scope.fields = [
    {
      'label': 'test label',
      'input': 'user.company'
    },
    {
      'label': 'test label',
      'input': 'user.company'
    }
  ];
}

})();
