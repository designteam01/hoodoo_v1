(function() {
  'use strict';

  angular
    .module('hoodooTwo')
    .config(routeConfig)
    .config(locationConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('services', {
        url: '/services',
        templateUrl: 'app/services/services.html',
        controller: 'ServicesController',
        controllerAs: 'services'
      })
      .state('portfolio', {
        url: '/portfolio',
        templateUrl: 'app/portfolio/portfolio.html',
        controller: 'PortfolioController',
        controllerAs: 'portfolio'
      })
      .state('contact', {
        url: '/contact',
        templateUrl: 'app/contact/contact.html',
        controller: 'ContactController',
        controllerAs: 'contact'
      })
      .state('referral', {
        url: '/referral',
        templateUrl: 'app/landing/referral/referral.html',
        controller: 'ReferralController',
        controllerAs: 'referral'
      })
      .state('blog', {
        url: '/blog',
        templateUrl: 'app/blog/blog.html',
        controller: 'BlogController',
        controllerAs: 'blog'
      })
      .state('promotions', {
        url: '/promotions',
        templateUrl: 'app/promotions/promotions.html',
        controller: 'PromoController',
        controllerAs: 'promotions'
      });
      // add referral link

    $urlRouterProvider.otherwise('/');
  }

  function locationConfig($locationProvider) {
    // enable html5Mode for pushstate ('#'-less URLs)
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');
  }

})();
