(function() {
  'use strict';

  angular
    .module('hoodooTwo')
    .controller('PromoController', PromoController);

  /** @ngInject */
  function PromoController($scope) {
    $scope.title = {
      'h1':'Promotions',
    };
    $scope.content = {
      'lead': 'At Hoodoo we appreciate our clients, both new and existing. We like to express that appreciation whenever we can.',
      'leadTwo': 'Please take an opportunity to take advantage of any promotional offers that may apply.'
    };
    $scope.sections = [
      {
        'head': 'Referral Bonus',
        'sub': '10% Referral Bonus',
        'p': 'Hoodoo values your time and would like to reward you for referring clients to us. If you happen to know of someone that is actively looking to embark upon a web development project, please reach out to use via email.',
        'disclaimer': '10% of total contract price up to a maximum of $500.'
      },
      {
        'head': 'New Client Discount',
        'sub': '10% Discount',
        'p': 'Hoodoo values your time and would like to reward you for referring clients to us. If you happen to know of someone that is actively looking to embark upon a web development project, please reach out to use via email.',
      },
      {
        'head': 'Military Discount',
        'sub': '5% Discount',
        'p': 'Hoodoo values your time and would like to reward you for referring clients to us. If you happen to know of someone that is actively looking to embark upon a web development project, please reach out to use via email.',
        'disclaimer': 'Discount applied in ADDITION to any other applicable discounts'
      }
    ];
  }
})();
