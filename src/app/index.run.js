(function() {
  'use strict';

  angular
    .module('hoodooTwo')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();

