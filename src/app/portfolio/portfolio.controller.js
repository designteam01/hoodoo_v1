(function() {

'use strict';

angular.module('hoodooTwo')
  .controller('PortfolioController', PortCtrl);

    function PortCtrl($scope) {
      $scope.title = {
        'h1':'Portfolio',
    };
    $scope.content = {
        'img':'assets/images/portfolioBG.jpg',
        'lead': 'Hoodoo takes an immense amount of pride in our projects. We are currently building our portfolio with projects coming quickly. Please be sure to check back and see what we are up to.'
    };
    $scope.sections = [
        {
            'h2': 'Testing',
            'p': 'Lorem ipsum Sunt ex amet irure nulla ea ullamco dolore officia labore mollit aute qui labore sunt veniam nisi aliqua magna ullamco ut labore sint laborum aliquip enim commodo Excepteur eu Ut ut velit non et et Duis dolore incididunt aliqua minim anim nisi dolor laborum laborum proident incididunt dolor commodo Duis in incididunt nostrud veniam exercitation veniam eiusmod in in voluptate esse ea nulla dolore ullamco in proident consequat adipisicing enim nostrud velit veniam dolore dolore magna et mollit laboris irure Ut adipisicing dolor ullamco velit minim ut pariatur labore reprehenderit reprehenderit.',
        },
        {
            'h2': 'Dev 2 Testing',
            'p': 'Mechanization network performance Bishop redundancy diode Robby. Hydraulic KARR interlock optimization shear pad aluminum degrees of freedom. Robot memory current drive train sensor acceleration laser beams gear titanium axle parallel hose glass singularity. Solar direct numerical control velocity iteration Chip system interlock solenoid. Program iteration magnet destroy Robby charging cam.',
        },
        {
            'h2': 'Testing',
            'p': 'commodo labore tempor amet fugiat culpa amet dolore sint quis dolore Duis velit adipisicing enim dolore reprehenderit sint culpa do in cillum in proident minim esse cupidatat eu culpa nostrud consequat ad adipisicing ea sint magna in fugiat est esse dolore magna proident culpa Ut consequat id minim anim nulla nostrud voluptate est sunt laborum est occaecat in minim velit dolor Ut amet ut ea ea Duis irure veniam ut fugiat nisi magna est in quis est nisi aute pariatur reprehenderit consequat quis Excepteur in laboris exercitation pariatur dolore dolore ex cillum sit id in exercitation reprehenderit laboris nisi Excepteur exercitation elit dolor aliqua dolore ut est culpa Ut Ut incididunt id ut voluptate adipisicing.'
        }
    ];
    $scope.projects = [
        {
            'img': 'http://qumani.com/BAY/assets/images/BAY.svg',
            'title': 'B.A.Y. Flag Football',
            'techs': [
              'Responsive',
              'HTML5',
              'CSS3',
              'Angular',
              'Bootstrap'
            ],
            'desc': 'Site for Bay Area Youth Flag Football. A local organization in the Bay Area for youth flag football league.',
            'launched': 'comingSoon'
        },
        {
            'img': 'assets/images/clientLogos/abcLogo.jpg',
            'title': 'Balesteri Construction',
            'techs': [
              'Responsive',
              'HTML5',
              'CSS3',
              'Angular',
              'Foundation',
              'Wordpress'
            ],
            'desc': 'Site for Balesteri Construction, who is a premier building contractor in the Monterey Bay and Penninsula.',
            'launched': 'comingSoon'
        },
        {
            'img': 'assets/images/clientLogos/hoodoo.jpg',
            'title': 'Hoodoo',
            'techs': [
              'Responsive',
              'HTML5',
              'CSS3',
              'Angular',
              'MaterialUI'
            ],
            'desc': 'This website will actually serve as our first portfolio piece. It is a responsive site built with a newer style referred to as Material Design.',
            'launched': '2015-06-01'
        }
    ];
    }
})();
