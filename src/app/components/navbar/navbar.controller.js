(function() {

'use strict';

angular.module('hoodooTwo')
  .controller('NavbarController', NavbarController)
  .controller('ToggleController', ToggleCtrl)
  .controller('LeftCtrl', LeftCtrl);


    function NavbarController($scope, $mdDialog) {
        $scope.links = [
            { 'url': 'home', 'label': 'home' },
            { 'url': 'services', 'label': 'services' },
            { 'url': 'portfolio', 'label': 'portfolio' },
            { 'url': 'contact', 'label': 'contact' }
        ];
        $scope.ctas = [
            // make list of CTA buttons for below the side navigation
            {
              'h2': 'Promotions',
              'p': 'View our current promotions',
              // 'desc': 'Hoodoo and its individual entities have a tremendous amount of respect and appreciation for service personnel. We offer a discount for active duty and retired military.' +
                // ' This discount is in ADDITION to any other applicable discounts.',
              'url': 'promotions'
            }
        ];
        $scope.modalCTAs = [
          //CTA modals
          {
            'h2': 'Want Some Money?',
            'p': 'Referral bonuses available.',
            'desc': 'Hoodoo values your time and would like to reward you for referring clients to us. If you happen to know of someone that is actively looking to embark upon a web development project please reach out to us on our contact page.',
            'link': 'contact'
          },
        ];

        var alert;
        var showDialog;
        $scope.showDialog = showDialog;
        // Internal method
        function dialog(){
            $mdDialog
            .show( alert )
            .finally(function() {
              alert = undefined;
            });
        }

        // cta dialog content population
        $scope.dialogAlert = function(idx){
            alert = $mdDialog.alert({
            title: $scope.modalCTAs[idx].h2,
            content: $scope.modalCTAs[idx].desc,
            ok: 'Close'
          });
          dialog();
        };
    }

    function ToggleCtrl($scope, $timeout, $mdSidenav, $mdUtil, $log) {
        $scope.toggleLeft = buildToggler('left');

        function buildToggler(navID) {
          var debounceFn =  $mdUtil.debounce(function(){
                $mdSidenav(navID)
                  .toggle()
                  .then(function () {
                    $log.debug("toggle " + navID + " is done");
                  });
              },300);

          return debounceFn;
        }
    }

    function LeftCtrl($scope, $timeout, $mdSidenav, $log) {
      $scope.close = function () {
        $mdSidenav('left').close()
          .then(function () {
            $log.debug("close LEFT is done");
          });

      };
    }
})();
