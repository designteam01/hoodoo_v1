(function() {

'use strict';

angular.module('hoodooTwo')
  .controller('ServicesController', ServicesCtrl);

    function ServicesCtrl($scope) {
      $scope.title = {
        'h1':'Services',
      };
      $scope.content = {
        'lead': 'Hoodoo offers a vast array of services from simple tweaks to existing sites to comrehensive branding packages. We are in the business of ensuring that your online or print projects convey the messaging and capabilities of your business.'
      };
      $scope.sections = [
        {
          'sub': 'Web Design',
          // 'p': 'Using the best-in-class technology, we create responsive websites and apps that perform. With a committed team of working on cutting-edge solutions, we customize interactive development for every client\’s need.'
          'p': 'Web design is generally the customer-facing part of the website. A web designer is concerned with how a site looks and how visitors interact with it. You can drill down further into "web design" with UI and UX development (explained more below). Web designers put together the principles of design to create a site that looks great. In addition, they understand usability and "user flow" making site navigation simple and "natural" feeling.'
        },
        {
          'sub': 'Web Development',
          'p': 'In contrast to "web design", development often refers to the backend of the site. Developers are more focused on how the site works rather than how it looks. Often developers utilize more complex scripting languages to setup interativity, and functions like form handling, access management, and data binding. Developers also generally work on things like analytics, CRM integrations, and data binding.'
        },
        {
          'sub': 'UI / UX',
          'p': 'User Interface (UI) and User Experience (UX) can sometimes be somewhat ambiguous terms. Though they both use a relative set of tools including, Photoshop, Illustrator, and Balsalmiq to name a few, there are distinct differences. UX design generally focuses on how the product "feels". Ensuring that things flow logically from one state or page to the next. UI designers are more concerned with how the page or project is laid out. They ensure that the UI visually communicates the UX design that has been laid out.',
        },
        {
          'sub': 'Business Identity and Branding',
          'p': 'Our team will provide a corporate brand and identity to display throughout all of your business channels. A full brand developed to showcase your identity, vision and company image. Some of these items include: Logo, Business Cards, Branded Materials (flyers, pamphlets, folders), PowerPoint Slide Decks.'
        },
        {
          'sub': 'Online Marketing',
          'p': 'Our team will provide a path to increase marketing outreach with email campaign templates, landing pages all designed to deliver results. We establish a series of customized approaches to bring qualified traffic to your site.'
        },
        {
          'sub': 'Copywriting',
          'p': 'Not sure what to say? That’s alright, we’ll provide web content to achieve your objectives and boosting your enquiries.  We’ll help with your content needs. We start with a conversation to understand you, your business and convey the perfect message.  '
        },
        {
          'sub': 'Web Maintenance',
          'p': 'We know your time is valuable, and we offer monthly maintenance plans to update your site.  This frees up your time and takes the weight off your shoulders so you can focus on your business.'
        }
      ];
    }
})();
