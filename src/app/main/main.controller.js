(function() {
  'use strict';

  angular
    .module('hoodooTwo')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, $scope) {
    $scope.title = {
      'h1':'Welcome',
    };
    $scope.content = {
      'lead': 'Web Development, Web Design, Online Marketing Solutions and Application Development - Delivering Results. Hoodoo\'s projects are responsive, radiant, and a must have business resource for reaching your audience.  Hoodoo is your Silicon Valley Web partner with finesse and laser execution on all projects!'
    };
    $scope.sections = [
      {
        'p': 'Our team will consult, propose and develop your website and other business needs to facilitate your immediate impactful impression to your visitors.  We are full of energy and excitement to provide the best project experience for our clients and see their businesses flourish. '
      },
      {
        'sub': 'About Us',
        'p': 'Hoodoo came together organically, with the desire to create and deliver a world-class web presence for all of our clients. We are a team infused with business, technology and creative professionals. We are a solid compact team bound with our love for delivering authentic web business solutions in all of our ventures.'
      }
    ];
  }
})();
