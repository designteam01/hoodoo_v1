(function() {

'use strict';

angular.module('hoodooTwo')
  .controller('BlogController', ContactCtrl);

function ContactCtrl($scope) {
    $scope.title = {
      'h1':'Contact Us'
    };
    $scope.content = {
      'img':'assets/images/hoodoo-contact.png',
      'lead': 'Please fill out the form below to get in contact with us regarding your potential product or any questions that you may have. Thanks again for visiting'
    };
    $scope.sections = [
      {
        'sub': 'Testing',
        'p': 'commodo labore tempor amet fugiat culpa amet dolore sint quis dolore Duis velit adipisicing enim dolore reprehenderit sint culpa do in cillum in proident minim esse cupidatat eu culpa nostrud consequat ad adipisicing ea sint magna in fugiat est esse dolore magna proident culpa Ut consequat id minim anim nulla nostrud voluptate est sunt laborum est occaecat in minim velit dolor Ut amet ut ea ea Duis irure veniam ut fugiat nisi magna est in quis est nisi aute pariatur reprehenderit consequat quis Excepteur in laboris exercitation pariatur dolore dolore ex cillum sit id in exercitation reprehenderit laboris nisi Excepteur exercitation elit dolor aliqua dolore ut est culpa Ut Ut incididunt id ut voluptate adipisicing.'
      }
    ];
  }
})();

